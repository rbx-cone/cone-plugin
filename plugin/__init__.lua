local HttpService = game:GetService('HttpService')

local pluginToolbar = plugin:CreateToolbar("cone")
local toggleButton = pluginToolbar:CreateButton("cone start", "Enable the cone-plugin", "rbxassetid://2155952150")
local refreshTime = 1

local Settings = require(script.Settings)

local automaticallyRefreshing = false

local Tasks = {}
for _, taskModule in ipairs(script.Tasks:GetChildren()) do
	Tasks[taskModule.Name] = require(taskModule)
end

toggleButton.Click:Connect(function()
	automaticallyRefreshing = not automaticallyRefreshing
	toggleButton:SetActive(automaticallyRefreshing)
	while automaticallyRefreshing do
		local success, task = pcall(function()
			local result = HttpService:GetAsync(Settings.UrlTask)
			return HttpService:JSONDecode(result or '{}')
		end)

		if success then
			if Tasks[task.task_name] then
				Tasks[task.task_name](task)
			end
		end
		wait(refreshTime)
	end
end)
