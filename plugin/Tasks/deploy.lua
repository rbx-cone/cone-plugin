local Cone = script.Parent.Parent

local ApplyConfiguration = require(Cone.ApplyConfiguration)
local Reconcile = require(Cone.Reconcile)
local RevertConfiguration = require(Cone.RevertConfiguration)
local Storage = require(Cone.Storage)
local SendResults = require(Cone.SendResults)
local Settings = require(Cone.Settings)

local function RemoveTests(source)
	for _, child in ipairs(source:GetDescendants()) do
		if child.Name:match('%.spec$') then
			child:Destroy()
		end
	end
end

return function(task)
	local lastConfiguration = Storage.GetValue(Settings.LastDeployedConfiguration)

	if lastConfiguration then
		RevertConfiguration(lastConfiguration)
	end

	local source = Storage.GetTemporaryFolder()

	Reconcile(task.project, source)

	if not task.include_tests then
		RemoveTests(source)
	end

	ApplyConfiguration(source, task.configuration)

	Storage.SetValue(Settings.LastDeployedConfiguration, task.configuration)

	source:Destroy()

	SendResults(task.id, 'Deploy success', '')
end
