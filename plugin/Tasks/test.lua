local Cone = script.Parent.Parent

local ApplyConfiguration = require(Cone.ApplyConfiguration)
local Reconcile = require(Cone.Reconcile)
local Storage = require(Cone.Storage)
local SendResults = require(Cone.SendResults)
local StringReporter = require(Cone.StringReporter)
local TestEZ = require(Cone.TestEZ)

local function ResetNames(changedNames)
	for object, name in pairs(changedNames) do
		object.Name = name
	end
end

local function StripSpecSuffix(name)
	return (name:gsub("%.spec$", ""))
end

local function GetTestezPath(module, root)
	root = root or game

	local path = {}
	local last = module

	while last ~= nil and last ~= root do
		table.insert(path, StripSpecSuffix(last.Name))
		last = last.Parent
	end

	return path
end

return function(task)
	local source = Storage.GetTemporaryFolder()
	source.Parent = game.ReplicatedStorage

	Reconcile(task.project, source)

    local objects = source:GetDescendants()

	local changedNames = ApplyConfiguration(source, task.configuration, true)

	local objectsMap = {}

	for _, object in ipairs(objects) do
		objectsMap[object] = true
	end

	local modulesToTest = {}

	for _, object in ipairs(objects) do
		if object:IsA('ModuleScript') and object.Name:match('%.spec$') then
			local root = object
			local nextRoot = root.Parent

			while objectsMap[nextRoot] do
				root = nextRoot
				nextRoot = root.Parent
			end

			local path = GetTestezPath(object, root)
			if #path == 0 then
				warn('\nobject', object:GetFullName(), ' ==== ', root:GetFullName())
				warn(string.rep('%s.', #path):format(unpack(path)), '\n')
			end

			table.insert(modulesToTest, {
				method = require(object),
				path = GetTestezPath(object, root)
			})
		end
	end

	table.sort(modulesToTest, function(a, b)
		return a.path[#a.path]:lower() < b.path[#b.path]:lower()
	end)

	local plan = TestEZ.TestPlanner.createPlan(modulesToTest)
	local results = TestEZ.TestRunner.runPlan(plan)

	local str, err = StringReporter.report(results)

	SendResults(task.id, str, err)

	for _, object in ipairs(objects) do
		if object then
			object:Destroy()
		end
	end

	source:Destroy()

	ResetNames(changedNames)
end
