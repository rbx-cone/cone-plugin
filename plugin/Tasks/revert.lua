local Cone = script.Parent.Parent

local Storage = require(Cone.Storage)
local RevertConfiguration = require(Cone.RevertConfiguration)
local SendResults = require(Cone.SendResults)
local Settings = require(Cone.Settings)

return function(task)
	local lastConfiguration = Storage.GetValue(Settings.LastDeployedConfiguration)

	if lastConfiguration then
		RevertConfiguration(lastConfiguration)
		Storage.DeleteValue(Settings.LastDeployedConfiguration)
		SendResults(task.id, 'Code reverted to last integration.', '')
	else
		SendResults(task.id, '', 'Nothing to revert.')
	end
end
