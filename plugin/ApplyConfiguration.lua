local HttpService = game:GetService('HttpService')

local function GetOrCreateParent(path)
	local parent = game

	for name in path:gmatch('[^%.]+') do
		if parent == game then
			parent = game:GetService(name)
		else
			local nextParent = parent:FindFirstChild(name)

			if not nextParent then
				nextParent = Instance.new('Folder')
				nextParent.Name = name
				nextParent.Parent = parent
			end

			parent = nextParent
		end
	end

	return parent
end

return function(source, configuration, renameSiblings)
    local changedNames = {}

	for _, objectConfig in ipairs(configuration) do
		local object = source

		for name in objectConfig.path:gmatch('[^/]+') do
			object = object:FindFirstChild(name:match('(.+)%.lua') or name)
		end

        local parent = GetOrCreateParent(objectConfig.parent)

        while renameSiblings and parent:FindFirstChild(object.Name) do
            local sibling = parent[object.Name]
            changedNames[sibling] = object.Name
            sibling.Name = HttpService:GenerateGUID(false)
        end

		object.Parent = parent
    end

    return changedNames
end
