local function GetScriptClass(fileName)
	if fileName:match('%.server') then
		return Instance.new('Script')
	elseif fileName:match('%.client') then
		return Instance.new('LocalScript')
	else
		return Instance.new('ModuleScript')
	end
end

local function Reconcile(project, parent)
	for key, value in pairs(project) do
		if type(value) == 'string' then
			local module = GetScriptClass(key)
			module.Source = value

			if key:match('^__init__%.lua') and parent:IsA('Folder') then
				module.Name = parent.Name
				module.Parent = parent.Parent
				for _, child in ipairs(parent:GetChildren()) do
					child.Parent = module
				end
				parent:Destroy()
				parent = module
			else
				module.Name = key:match('(.+)%.lua$') or 'unknown'
				module.Parent = parent
			end
		else
			local folder = Instance.new('Folder')
			folder.Name = key
			folder.Parent = parent
			Reconcile(value, folder)
		end
	end
end

return Reconcile
