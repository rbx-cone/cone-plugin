local HttpService = game:GetService('HttpService')
local ServerStorage = game:GetService('ServerStorage')

local Cone = script.Parent
local Settings = require(Cone.Settings)

local function GetConeFolder()
	local coneFolder = ServerStorage:FindFirstChild(Settings.ConeFolder)

	if not coneFolder then
		coneFolder = Instance.new('Folder')
		coneFolder.Name = Settings.ConeFolder
		coneFolder.Parent = ServerStorage
	end

	return coneFolder
end

local function GetValue(name, default)
	local coneFolder = GetConeFolder()

	local value = coneFolder:FindFirstChild(name)

	if value and value:IsA('StringValue') then
		value = HttpService:JSONDecode(value.Value)
	end

	if value == nil then
		return default
	else
		return value
	end
end

local function SetValue(name, value)
	local coneFolder = GetConeFolder()

	local valueObject = coneFolder:FindFirstChild(name)

	if not valueObject then
		valueObject = Instance.new('StringValue')
		valueObject.Name = name
		valueObject.Parent = coneFolder
	end

	valueObject.Value = HttpService:JSONEncode(value)
end

local function DeleteValue(name)
	local coneFolder = GetConeFolder()

	local valueObject = coneFolder:FindFirstChild(name)

	if valueObject then
        valueObject:Destroy()
    end
end

local function GetTemporaryFolder(parent)
	local folder = Instance.new('Folder')
	folder.Name = 'TEMP'
	return folder
end

return {
	GetConeFolder = GetConeFolder,
	GetTemporaryFolder = GetTemporaryFolder,
	GetValue = GetValue,
    SetValue = SetValue,
    DeleteValue = DeleteValue,
}
