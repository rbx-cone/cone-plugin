local BaseUrl = 'http://localhost:5000/'

return {
	UrlTask = BaseUrl .. 'task',
	UrlResults = BaseUrl .. 'results/%s',

    ConeFolder = 'CONE',

    LastDeployedConfiguration = 'LastDeployedConfiguration'
}
