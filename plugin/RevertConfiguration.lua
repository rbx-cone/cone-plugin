local function GetParent(path)
	local parent = game
	for name in path:gmatch('[^%.]+') do
		if parent == game then
			parent = game:GetService(name)
		else
			parent = parent:FindFirstChild(name)
		end
	end
	return parent
end

return function(configuration)
	for _, objectConfig in ipairs(configuration) do
		local name = objectConfig.path:match('([^/]+)$')
		local nameWithoutLua = name:match('([^/]+)%.lua')

		if nameWithoutLua then
			name = nameWithoutLua
		end

		local parent = GetParent(objectConfig.parent)

		if parent then
			local object = parent:FindFirstChild(name)

			if object then
				object:destroy()
			end
		end
	end
end
