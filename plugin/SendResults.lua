local HttpService = game:GetService('HttpService')

local Cone = script.Parent
local Settings = require(Cone.Settings)

return function(id, str, err)
	local data = {
		out = str,
		error_out = err
	}
	pcall(function()
		HttpService:PostAsync(
			Settings.UrlResults:format(id),
			HttpService:JSONEncode(data)
		)
	end)
end