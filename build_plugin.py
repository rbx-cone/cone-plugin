import os
from item_classes import Root, Script, ModuleScript, Folder

# settings
SOURCE = "plugin"
EXPORT_DIRECTORY = "builds"
EXPORT_NAME = "coneplugin"

IGNORE_FILES = ["__init__.lua", "init.lua"]

root = Root()
pluginFolder = Script("cone-plugin", open(SOURCE + "/__init__.lua", 'r').read())
root.addChild(pluginFolder)


def getContent(directory, parent):
    for filename in os.listdir(directory):
        if filename.endswith(".lua"):
            if filename not in IGNORE_FILES:
                moduleScript = ModuleScript(filename[:-4], open(directory + "/" + filename, 'r').read())
                parent.addChild(moduleScript)

        else:
            if os.path.isfile(directory + "/" + filename + "/__init__.lua"):
                nextParent = Script("cone-plugin", open(SOURCE + "/__init__.lua", 'r').read())
                parent.addChild(nextParent)
                getContent(directory + "/" + filename, nextParent)
            else:
                nextParent = Folder(filename)
                parent.addChild(nextParent)
                getContent(directory + "/" + filename, nextParent)


def getFileName(n):
    if n == 0:
        return EXPORT_DIRECTORY + "/" + EXPORT_NAME + ".rbxmx"
    else:
        return EXPORT_DIRECTORY + "/" + EXPORT_NAME + " (" + str(n) + ").rbxmx"

getContent(SOURCE, pluginFolder)

# get TestEZ
testezFolder = "modules/testez/lib"
testEZ = ModuleScript("TestEZ", open(testezFolder + "/init.lua", 'r').read())
getContent(testezFolder, testEZ)
pluginFolder.addChild(testEZ)

if not os.path.exists(EXPORT_DIRECTORY):
    os.mkdir(EXPORT_DIRECTORY)

n = 0
name = getFileName(n)
while os.path.exists(name):
    n += 1
    name = getFileName(n)

data = root.getData()
with open(name, 'w') as file:
    file.write(data)
    print("File " + name + " exported.")

if os.path.exists('build_locations.txt'):
    other_locations = [line.rstrip('\n') for line in open('build_locations.txt')]
    for location in other_locations:
        with open(location + '/' + EXPORT_NAME + '.rbxmx', 'w') as file:
            file.write(data)
