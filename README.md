# __CONE__

__cone__ is a tool to simplify your workflow when you are working on Roblox. It comes it two parts:
 - A python package named [`cone`](https://gitlab.com/rbx-cone/cone)
 - A Roblox plugin also named `cone-plugin`

Get more information about `cone` on the [package project](https://gitlab.com/rbx-cone/cone)

You can download cone from the [Roblox page](https://www.roblox.com/library/2125264005).